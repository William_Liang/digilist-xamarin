﻿using Digilist.Enums;
using Digilist.Helpers;
using Digilist.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist.Repositories
{
    public class DigimonRepository
    {
        private static DBHelper DB = new DBHelper();

        public static ObservableCollection<Digimon> GetAllRookieDigimons()
        {
            return new ObservableCollection<Digimon>((from Digimon in DB.Database.Table<Digimon>()
                                                      where Digimon.DigimonLevel.Equals(DigimonLevels.Rookie)
                                                      select Digimon).ToList<Digimon>());
        }

        public static ObservableCollection<Digimon> GetAllDigimons()
        {
            return new ObservableCollection<Digimon>(DB.Database.Table<Digimon>());
        }

        public static Digimon GetDigimon(String DigimonName)
        {
            ObservableCollection<Digimon> AllDigimons = GetAllDigimons();
            foreach (Digimon Digimon in AllDigimons)
            {
                if (Digimon.DigimonName.Equals(DigimonName))
                {
                    return Digimon;
                }
            }
            return null;
        }
    }
}
