﻿using Digilist.Enums;
using Digilist.Helpers;
using Digilist.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist.Repositories
{
    public class TamerRepository
    {
        private static DBHelper DB = new DBHelper();
        
        public static Tamer CreateTamer(String TamerName, Gender TamerGender, DateTime TamerBirthDate)
        {
            List<DigimonEvolution> TamerDigimonMappings = new List<DigimonEvolution>();
            Tamer NewTamer = new Tamer() { TamerName = TamerName,
                                            TamerGender = TamerGender,
                                            TamerBirthDate = TamerBirthDate };
            
            DB.Database.Insert(NewTamer);
            return NewTamer;
        }

        public static Tamer GetSessionTamer()
        {
            int? TamerId = (int?)PropertiesHelper.GetProperty(Constants.TAMER_PROPERTY_NAME);
            if (TamerId != null)
            {
                return DB.Database.Table<Tamer>().Where(t => t.TamerId == TamerId).FirstOrDefault();
            }
            return null;
        }

        public static void SetSessionTamer(Tamer CurrentTamer)
        {
            PropertiesHelper.SaveProperty(Constants.TAMER_PROPERTY_NAME, CurrentTamer.TamerId);
        }
    }
}
