﻿using Digilist.Helpers;
using Digilist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist.Repositories
{
    public class DigimonEvolutionRepository
    {
        private static DBHelper DB = new DBHelper();

        public static List<Digimon> GetDigimonEvolutions(Digimon StartDigimon)
        {
            List<Digimon> EvolutionDigimons = new List<Digimon>() { StartDigimon };
            while (true)
            {
                DigimonEvolution Evolution = (from d in DB.Database.Table<DigimonEvolution>()
                                              where d.BeforeEvolveId == StartDigimon.DigimonId
                                              select d).FirstOrDefault();
                if (Evolution != null)
                {
                    Digimon NextDigimon = (from d in DB.Database.Table<Digimon>()
                                           where d.DigimonId == Evolution.AfterEvolveId
                                           select d).FirstOrDefault();
                    if (NextDigimon != null)
                    {
                        EvolutionDigimons.Add(NextDigimon);
                        StartDigimon = NextDigimon;
                    }
                    else
                        break;
                }
                else
                    break;
            }
            return EvolutionDigimons;
        }
    }
}
