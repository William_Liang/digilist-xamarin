﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist
{
    public class Constants
    {
        public static readonly String ANDROID_RESOURCE_PATH = "Resources/drawable/";
        public static readonly String WINPHONE_RESOURCE_PATH = "Assets/";
        public static readonly String IOS_RESOURCE_PATH = "Resources/";

        public static readonly String TAMER_PROPERTY_NAME = "TAMER_DATA";

        public static readonly String DATABASE_NAME = "digilist-db";
    }
}
