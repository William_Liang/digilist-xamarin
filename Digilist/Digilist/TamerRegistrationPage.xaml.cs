﻿using Digilist.Enums;
using Digilist.Helpers;
using Digilist.Models;
using Digilist.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Digilist
{
    public partial class TamerRegistrationPage : ContentPage
    {
        public TamerRegistrationPage()
        {
            InitializeComponent();
        }

        public async void BtnRegister_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TamerName.Text))
            {
                await DisplayAlert("Error", "Name must be filled", "OK");
            }
            else if (DateTime.Now.Year - TamerBirthDate.Date.Year < 17)
            {
                await DisplayAlert("Error", "You must be at least 17 years old", "OK");
            }
            else
            {
                Gender SelectedGender = EnumHelper.GetEnumValue<Gender>(TamerGender.Items[TamerGender.SelectedIndex].ToString());
                
                Tamer NewTamer = TamerRepository.CreateTamer(TamerName.Text, SelectedGender, TamerBirthDate.Date);

                TamerRepository.SetSessionTamer(NewTamer);
                await Navigation.PushAsync(new DigimonListPage(NewTamer));
            }
        }
    }
}
