﻿using Digilist.Enums;
using Digilist.Interfaces;
using Digilist.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Digilist.Helpers
{
    public class DBHelper
    {
        public SQLiteConnection Database { get; set; }

        public DBHelper()
        {
            if (Database == null)
            {
                Database = DependencyService.Get<ISQLite>().GetConnection();
            }
        }

        public void Init()
        {
            Database.DropTable<Digimon>();
            Database.DropTable<DigimonEvolution>();

            Database.CreateTable<Tamer>();
            Database.CreateTable<Digimon>();
            Database.CreateTable<DigimonEvolution>();

            InitData();
        }

        private void InitData()
        {
            List<List<Digimon>> AllDigimonsList = new List<List<Digimon>> { { new List<Digimon> { new Digimon { DigimonName="Agumon", DigimonType=DigimonTypes.Fire, DigimonLevel=DigimonLevels.Rookie, DigimonImage="Agumon.png" },
                                                                                                    new Digimon { DigimonName="Greymon", DigimonType=DigimonTypes.Fire, DigimonLevel=DigimonLevels.Champion, DigimonImage="Agumon.png" },
                                                                                                    new Digimon { DigimonName="MetalGreymon", DigimonType=DigimonTypes.Fire, DigimonLevel=DigimonLevels.Ultimate, DigimonImage="Agumon.png"},
                                                                                                    new Digimon { DigimonName="Wargreymon", DigimonType=DigimonTypes.Fire, DigimonLevel=DigimonLevels.Mega, DigimonImage="Agumon.png" } } },

                                                                            { new List<Digimon> { new Digimon { DigimonName="Gabumon", DigimonType=DigimonTypes.Water, DigimonLevel=DigimonLevels.Rookie, DigimonImage="Gabumon.jpg" },
                                                                                                    new Digimon { DigimonName="Garurumon", DigimonType=DigimonTypes.Water, DigimonLevel=DigimonLevels.Champion, DigimonImage="Gabumon.jpg" },
                                                                                                    new Digimon { DigimonName="Weregarurumon", DigimonType=DigimonTypes.Water, DigimonLevel=DigimonLevels.Ultimate, DigimonImage="Gabumon.jpg"},
                                                                                                    new Digimon { DigimonName="MetalGarurumon", DigimonType=DigimonTypes.Water, DigimonLevel=DigimonLevels.Mega, DigimonImage="Gabumon.jpg" } } } };

            int DigimonId = 1;
            foreach (List<Digimon> Digimons in AllDigimonsList)
            {
                foreach (Digimon Digimon in Digimons)
                {
                    Database.Insert(Digimon);
                    Digimon.DigimonId = DigimonId++;
                }

                for (int i = 0; i < Digimons.Count - 1; i++)
                {
                    DigimonEvolution DE = new DigimonEvolution(Digimons[i], Digimons[i + 1]);
                    Database.Insert(DE);
                }
            }
        }
    }
}
