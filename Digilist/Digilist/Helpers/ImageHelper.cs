﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Digilist.Helpers
{
    public class ImageHelper
    {
        public static String GetImageByPlatform(String ImageLink)
        {
            switch (Device.OS)
            {
                case TargetPlatform.Other:
                    return null;
                case TargetPlatform.iOS:
                    if (ImageLink.StartsWith(Constants.IOS_RESOURCE_PATH))
                        ImageLink = ImageLink.Substring(Constants.IOS_RESOURCE_PATH.Length);
                    return Constants.IOS_RESOURCE_PATH + ImageLink;

                case TargetPlatform.Android:
                    if (ImageLink.StartsWith(Constants.ANDROID_RESOURCE_PATH))
                        ImageLink = ImageLink.Substring(Constants.ANDROID_RESOURCE_PATH.Length);
                    return Constants.ANDROID_RESOURCE_PATH + ImageLink;

                case TargetPlatform.WinPhone:
                    if (ImageLink.StartsWith(Constants.WINPHONE_RESOURCE_PATH))
                        ImageLink = ImageLink.Substring(Constants.WINPHONE_RESOURCE_PATH.Length);
                    return Constants.WINPHONE_RESOURCE_PATH + ImageLink;

            }
            return "";
        }
    }
}
