﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist.Enums
{
    public class EnumHelper
    {
        public static TEnum GetEnumValue<TEnum>(String Text) where TEnum:struct
        {
            if (!String.IsNullOrEmpty(Text))
            {
                TEnum result = default(TEnum);
                if (Enum.TryParse<TEnum>(Text, out result))
                    return result;
            }
            return default(TEnum);
        }
    }
}
