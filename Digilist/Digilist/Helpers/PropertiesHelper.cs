﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Digilist.Helpers
{
    public class PropertiesHelper
    {
        public static void SaveProperty(String Name, Object Value)
        {
            Application.Current.Properties[Name] = Value;
        }

        public static Object GetProperty(String Name)
        {
            if (Application.Current.Properties.ContainsKey(Name))
            {
                return Application.Current.Properties[Name];
            }
            return null;
        }
    }
}
