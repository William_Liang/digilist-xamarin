﻿using Digilist.Enums;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist.Models
{
    [Table("Tamers")]
    public class Tamer
    {
        [PrimaryKey, AutoIncrement]
        public int TamerId { get; set; }
        public String TamerName { get; set; }
        public Gender TamerGender { get; set; }
        public DateTime TamerBirthDate { get; set; }
    }
}
