﻿using Digilist.Enums;
using Digilist.Helpers;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Digilist.Models
{
    [Table("Digimons")]
    public class Digimon
    {
        private String _DigimonImage;

        [PrimaryKey, AutoIncrement]
        public int DigimonId { get; set; }
        public String DigimonName { get; set; }
        public DigimonTypes DigimonType { get; set; }
        public DigimonLevels DigimonLevel { get; set; }
        public String DigimonImage
        {
            get { return ImageHelper.GetImageByPlatform(_DigimonImage); }
            set { this._DigimonImage = value; }
        }
    }
}
