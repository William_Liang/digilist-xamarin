﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist.Models
{
    [Table("DigimonEvolution")]
    public class DigimonEvolution
    {
        [PrimaryKey, AutoIncrement]
        public int MappingId { get; set; }
        public int BeforeEvolveId { get; set; }
        public int AfterEvolveId { get; set; }

        public DigimonEvolution()
        {

        }
        public DigimonEvolution(Digimon BeforeEvolve, Digimon AfterEvolve)
        {
            this.BeforeEvolveId = BeforeEvolve.DigimonId;
            this.AfterEvolveId = AfterEvolve.DigimonId;
        }
    }
}
