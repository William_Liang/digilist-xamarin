﻿
using Digilist.Models;
using Digilist.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Digilist
{
    public partial class DigimonListPage : ContentPage
    {
        public DigimonListPage(Tamer Tamer)
        {
            InitializeComponent();
            DigimonListView.ItemsSource = DigimonRepository.GetAllRookieDigimons();
        }
        
        public async void OnDigimonTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item != null)
            {
                Digimon SelectedDigimon = (Digimon)e.Item;
                DigimonDetailPage DetailPage= new DigimonDetailPage(SelectedDigimon);
                await Navigation.PushAsync(DetailPage);
            }
        }
    }
}
