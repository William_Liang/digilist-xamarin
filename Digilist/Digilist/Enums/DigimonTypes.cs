﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist.Enums
{
    public enum DigimonTypes
    {
        Fire,
        Water,
        Earth,
        Wind
    }
}
