﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digilist.Enums
{
    public enum DigimonLevels
    {
        Rookie,
        Champion,
        Ultimate,
        Mega
    }
}
