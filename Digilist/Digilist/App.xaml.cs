﻿using Digilist.Enums;
using Digilist.Helpers;
using Digilist.Models;
using Digilist.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Digilist
{
    public partial class App : Application
    {
        Tamer CurrentTamer;
        DBHelper DB = new DBHelper();

        public App()
        {
            InitializeComponent();

            DB.Init();

            if ((CurrentTamer = TamerRepository.GetSessionTamer()) != null)
            {
                MainPage = new NavigationPage(new DigimonListPage(CurrentTamer));
            }
            else
            {
                MainPage = new NavigationPage(new TamerRegistrationPage());
            }
        }
    }
}
