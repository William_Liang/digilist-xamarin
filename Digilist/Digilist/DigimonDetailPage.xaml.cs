﻿using Digilist.Models;
using Digilist.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Digilist
{
    public partial class DigimonDetailPage : CarouselPage
    {
        public DigimonDetailPage(Digimon SelectedDigimon)
        {
            InitializeComponent();
            this.ItemsSource = DigimonEvolutionRepository.GetDigimonEvolutions(SelectedDigimon);
        }
    }
}
