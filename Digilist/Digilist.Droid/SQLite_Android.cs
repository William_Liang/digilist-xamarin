using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Digilist.Interfaces;
using SQLite;
using System.IO;
using Digilist.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite_Android))]
namespace Digilist.Droid
{
    public class SQLite_Android : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            String DocumentPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            String FinalPath = Path.Combine(DocumentPath, Constants.DATABASE_NAME);
            return new SQLiteConnection(FinalPath);
        }
    }
}