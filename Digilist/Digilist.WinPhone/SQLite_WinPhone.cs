﻿using Digilist.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Digilist.WinPhone;
using Xamarin.Forms;
using System.IO;
using Windows.Storage;

[assembly: Dependency(typeof(SQLite_WinPhone))]
namespace Digilist.WinPhone
{
    public class SQLite_WinPhone : ISQLite
    {
        public SQLiteConnection GetConnection()
        {
            String DocumentPath = ApplicationData.Current.LocalFolder.Path;
            String FinalPath = Path.Combine(DocumentPath, Constants.DATABASE_NAME);
            return new SQLiteConnection(FinalPath);
        }
    }
}
